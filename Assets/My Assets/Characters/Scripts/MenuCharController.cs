﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kontroler postaci (w Menu).
/// </summary>
public class MenuCharController : MonoBehaviour {

	private Quaternion m_targetRotation; //Docelowa rotacja
	/// <summary>
	/// Obiekt broni postaci.
	/// </summary>
	public GunController m_gun;
	private Camera m_cam; //Kamera

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		m_cam = Camera.main;
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update ()
	{
		//Zrzutowanie pozycji myszy z ekranu na świat gry
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = m_cam.nearClipPlane;
		Vector3 worldPos = m_cam.ScreenToWorldPoint (mousePos);

		m_gun.AimTo (worldPos, false);
		SetTargetRotation (worldPos - this.transform.position);
	}

	/// <summary>
	/// Ustala docelową rotację obiektu.
	/// </summary>
	/// <param name="targetPos">Punkt, w kierunku którego powinien być skierowany obiekt.</param>
	public void SetTargetRotation(Vector3 targetPos)
	{
		targetPos.y = 0.0f;
		if (targetPos.magnitude > 0.1)
			m_targetRotation = Quaternion.LookRotation (targetPos);
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		transform.rotation = Quaternion.RotateTowards(transform.rotation, m_targetRotation, 360 * Time.deltaTime);
	}
}
