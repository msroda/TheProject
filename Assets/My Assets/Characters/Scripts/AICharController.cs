﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))] //Do ustalanie ścieżki poruszania
[RequireComponent(typeof (CharController))] //Postać do kontrolowania

/// <summary>
/// Kontroler postaci (SI).
/// </summary>
public class AICharController : MonoBehaviour
{
	/// <summary>
	/// Sposób wyboru ścieżki patrolu.
	/// </summary>
	public enum PatrolType
	{
		/// <summary>
		/// W pętli.
		/// </summary>
		LOOP,
		/// <summary>
		/// Tam i z powrotem
		/// </summary>
		BACKANDFORTH
	}

	/// <summary>
	/// Tryb działania SI.
	/// </summary>
	public enum State
	{
		/// <summary>
		/// Patrol.
		/// </summary>
		PATROL,
		/// <summary>
		/// Podążanie za graczem.
		/// </summary>
		FOLLOW,
		/// <summary>
		/// Badanie miejsca, w którym ostantio widziano gracza.
		/// </summary>
		ALARMED
	}

	/// <summary>
	/// Sposób wyboru ścieżki patrolu.
	/// </summary>
	public PatrolType m_patrolType;
	private bool m_patrolDirection; //Kierunek ścieżki podczas patrolu tam i z powrotem

	/// <summary>
	/// Czas oczekiwania w punkcie patrolu.
	/// </summary>
	public float m_patrolWaitTime;
	private float m_patrolWaitTimer = 0.0f; //Licznik odmierzający czas oczekiwania w punkcie podczas aptrolu
	private bool m_patrolShouldWait = false; //Czy postać ma jeszcze czekać
	private bool m_patrolWaitTimerOn = false; //Włączenie/wyłączenie licznika

	private State m_state; //Aktualny tryb działania SI
	private NavMeshAgent m_agent; //Komponent odpowiedzialny za wyznaczanie ścieżki do celu
	private CharController m_character; //Obiekt postaci do kontrolowania
	private bool m_alive; //Czy postać żyje
	/// <summary>
	/// Aktualny stan życia.
	/// </summary>
	public float m_health = 100.0f;

	/// <summary>
	/// Kolejne punkty do odwiedzenia podczas patrolu.
	/// </summary>
	public GameObject[] m_waypoints;
	private int m_waypointIndex = 0; //Indeks wskazujący aktualny punkt docelowy

	/// <summary>
	/// Prędkość poruszania postacią podczas patrolu.
	/// </summary>
	public float m_walkingSpeed = 0.5f;
	/// <summary>
	/// Prędkość poruszania postacią podczas pościgu lub alarmu.
	/// </summary>
	public float m_runningSpeed = 1.0f;

	private Vector3 m_lastSeenPosition; //Miejsce, w którym ostatnio widziano postać gracza
	private float m_alarmTimer = 0.0f; //Licznik odmierzający długość stanu alarmowego

	/// <summary>
	/// Czas trwania stanu alarmowego.
	/// </summary>
	public float m_alarmLength = 10.0f;
	private bool m_alarmTimerOn = false; //Włączenie/wyłączenie licznika
	private int m_alarmStep; //Aktualny etap procedury alarmowej
	private bool m_hitPortal; //Czy podczas aktualnego stanu alarmowego postać SI przeszła przez portal

	private float m_freezeTimer = 0.0f; //Licznik odmierzający czas trwania spowolnienia
	/// <summary>
	/// Czas trwania spowolnienia.
	/// </summary>
	public float m_freezeLength = 3.0f;
	private bool m_freezeTimerOn = false; //Włączenie/wyłączenie licznika

	/// <summary>
	/// Mnożnik prędkości podczas spowolnienia.
	/// </summary>
	public float m_freezeSpeedMultiplayer = 0.5f;

	private float m_shockTimer = 0.0f; //Licznik odmierzający czas trwania szoku elektrycznego

	/// <summary>
	/// Czas trwania szoku elektrycznego.
	/// </summary>
	public float m_shockLength = 2.0f;
	private bool m_shockTimerOn = false; //Włączenie/wyłączenie licznika
	private bool m_shocked = false; //Czy postać jest w szoku5

	/// <summary>
	/// Obiekt broni.
	/// </summary>
	public GameObject m_gun; //Obiekt broni
	private float m_shootCooldownTimer = 0.0f; //Licznik odmierzający czas, po którym może nastąpić następny strzał
	/// <summary>
	/// Czas, po którym może nastąpić następny strzał.
	/// </summary>
	public float m_shootingCooldown = 1.0f;
	private bool m_isOnCooldown = false; //Czy można strzelić
	/// <summary>
	/// Prędkość wystrzeliwanych pocisków.
	/// </summary>
	public float m_bulletVelocity = 15.0f;

	/// <summary>
	/// Obiekt gracza.
	/// </summary>
	public GameObject m_player;
	private Collider m_playerColl; //Kolider obiektu gracza
	/// <summary>
	/// Kamera będąca wzrokiem postaci.
	/// </summary>
	public Camera m_camera;
	private Plane[] m_planes; //Pole widzenia kamery
	private bool m_seesPlayer; //Czy postać widzi obiekt gracza
	private float m_speedMultiplayer = 1.0f; //Aktualny mnożnik prędkości postaci

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		m_agent = GetComponent<NavMeshAgent>();
		m_character = GetComponent<CharController>();
		m_playerColl = m_player.GetComponent<CapsuleCollider>();

		m_agent.updatePosition = true; //Agent ma ustalać pozycję postaci,
		m_agent.updateRotation = false; //ale jej rotacja jest ustalana przez kontroler.

		m_state = State.PATROL; 
		m_alive = true;

		StartCoroutine("FSM");
	}

	/// <summary>
	/// Wywołanie odpowiedniego trybu działania SI.
	/// </summary>
	IEnumerator FSM()
	{
		while (m_alive)
		{
			switch (m_state)
			{
			case State.PATROL:
				Patrol();
				break;
			case State.FOLLOW:
				Follow();
				break;
			case State.ALARMED:
				Alarmed();
				break;
			}

			yield return null;
		}

	}

	/// <summary>
	/// Wyznaczenie indeksu kolejnego punktu docelowego podczas patrolu.
	/// </summary>
	void NextWaypoint()
	{
		//W zależności od wybranego typu
		switch (m_patrolType)
		{
		//W pętli
		case (PatrolType.LOOP):
			m_waypointIndex++;
			if (m_waypointIndex >= m_waypoints.Length)
				m_waypointIndex = 0;
			break;
		//Tam i z powrotem
		case (PatrolType.BACKANDFORTH):
			//W zależności od obecnego kierunku
			if (m_patrolDirection)
			{
				if (m_waypointIndex >= m_waypoints.Length - 1)
				{
					m_waypointIndex = m_waypoints.Length - 2;
					m_patrolDirection = false;
				}
				else
					m_waypointIndex++;
				break;
			} 
			else 
			{
				if (m_waypointIndex <= 0)
				{
					m_waypointIndex = 1;
					m_patrolDirection = true;
				}
				else
					m_waypointIndex--;
				break;
			}

		}
	}

	/// <summary>
	/// Obsługa działania SI w trybie patrolu.
	/// </summary>
	void Patrol()
	{
		m_alarmTimer = 0.0f; //reset licznika czasu alarmu
		m_alarmStep = 0; //reset indeksu procedury alarmu
		m_alarmTimerOn = false; //wyłączeniu licznika czasu alarmu
		m_agent.speed = m_walkingSpeed * m_speedMultiplayer; //ustalenie prędkości poruszania 

		//Jeśli postać nie jest jeszcze wystarczająco blisko punktu docelowego?
		if (Vector3.Distance (this.transform.position, m_waypoints [m_waypointIndex].transform.position) >= 1)
		{
			m_agent.SetDestination(m_waypoints [m_waypointIndex].transform.position); //Ustal punkt docelowy
			m_character.SetTargetRotation (m_agent.desiredVelocity); 
			m_character.Move (m_agent.desiredVelocity, false, false);
			m_patrolShouldWait = true; //Reset zmiennej, by po osiągnięciu miejsca docelowego SI czekała
		}
		else
		{
			//Czekanie
			if (m_patrolShouldWait)
			{
				m_patrolWaitTimerOn = true;
				if (m_patrolWaitTimer >= m_patrolWaitTime)
				{
					m_patrolWaitTimerOn = false;
					m_patrolWaitTimer = 0.0f;
					m_patrolShouldWait = false;
				}
			}
			else 
			{
				NextWaypoint();
				m_patrolShouldWait = true;
			}
		} 

	}

	/// <summary>
	/// Obsługa działania SI w trybie pościgu.
	/// </summary>
	void Follow()
	{
		m_alarmTimer = 0.0f; //reset licznika czasu alarmu
		m_alarmStep = 0; //reset indeksu procedury alarmu
		m_alarmTimerOn = false; //wyłączeniu licznika czasu alarmu
		m_agent.speed = m_runningSpeed * m_speedMultiplayer; //ustalenie prędkości poruszania 
		//Jeśli obiekt gracza jest blisko, zmniejszenie prędkości
		if (Vector3.Distance (this.transform.position, m_player.transform.position) < 10.0f)
		{
			m_agent.speed = m_runningSpeed * m_speedMultiplayer * 0.5f;
		}
		//Jeśli obiekt gracza jest bardzo blisko, zatrzmyanie
		if (Vector3.Distance (this.transform.position, m_player.transform.position) < 1.5f)
		{
			m_agent.speed = 0.0f;
		}
		m_agent.SetDestination(m_lastSeenPosition);
		if(m_agent.desiredVelocity.magnitude == 0.0f)
			m_character.SetTargetRotation (m_lastSeenPosition - this.transform.position);
		else
			m_character.SetTargetRotation (m_agent.desiredVelocity);
		m_character.Move (m_agent.desiredVelocity, false, false);
	}

	/// <summary>
	/// /Obsługa działania SI w trybie alarmu.
	/// </summary>
	void Alarmed()
	{
		GameObject[] playerPortals = GameObject.FindGameObjectsWithTag("Portal");

		m_agent.speed = m_runningSpeed * m_speedMultiplayer;

		//Jeśli miejsce, w którym ostatnio widziano gracza, jest daleko - SI podąża w jego kierunku
		if (Vector3.Distance (this.transform.position, new Vector3(m_lastSeenPosition.x, this.transform.position.y, m_lastSeenPosition.z)) >= 1)
		{
			m_agent.SetDestination (m_lastSeenPosition);

			//Jeśli miejsce jest nieosiągalne
			if (m_agent.desiredVelocity.magnitude < 0.1f)
				m_lastSeenPosition = this.transform.position;
			else
			{
				m_character.SetTargetRotation (m_agent.desiredVelocity);
				m_character.Move (m_agent.desiredVelocity, false, false);
			}
		}
		//Jeśli w pobliżu znajduje się portal, należy przez niego przejść
		else if(Vector3.Distance(playerPortals[0].transform.position, this.transform.position) < 2.0f && !m_hitPortal)
		{
			m_agent.SetDestination (playerPortals[0].transform.position);
			m_character.SetTargetRotation (m_agent.desiredVelocity);
			m_character.Move (m_agent.desiredVelocity, false, false);
			//Jeśli portal jest nieosiąglany
			if(m_agent.desiredVelocity.magnitude < 0.1f)
				m_hitPortal = true;		
		}
		//Jeśli w pobliżu znajduje się portal, należy przez niego przejść
		else if(Vector3.Distance(playerPortals[1].transform.position, this.transform.position) < 2.0f && !m_hitPortal)
		{
			m_agent.SetDestination (playerPortals[1].transform.position);
			m_character.SetTargetRotation (m_agent.desiredVelocity);
			m_character.Move (m_agent.desiredVelocity, false, false);
			//Jeśli portal jest nieosiąglany
			if(m_agent.desiredVelocity.magnitude < 0.1f)
				m_hitPortal = true;	
		}
		//W przeciwnym wypadku, rozglądaj się na boki
		else
		{
			m_agent.SetDestination (this.transform.position);
			m_alarmTimerOn = true;
			//Z określoną częstotliwością ustawiana jest nowa docelowa rotacja
			if (m_alarmTimer >= m_alarmLength / 5)
			{
				switch (m_alarmStep)
				{
				case 0: 
					m_character.SetTargetRotation (m_character.transform.right);
					break;
				case 1: 
					m_character.SetTargetRotation (-m_character.transform.right);
					break;
				case 2: 
					m_character.SetTargetRotation (-m_character.transform.right);
					break;
				case 3: 
					m_character.SetTargetRotation (m_character.transform.right);
					break;
				case 4: 
					m_state = State.PATROL;
					break;
				}
				m_alarmStep++;
				m_alarmTimer = 0.0f;
			}
			m_character.Move (Vector3.zero, false, false);

		}
	}

	/// <summary>
	/// Wywołanie zdarzenia wejśćia obiektu w kolizję z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerEnter(Collider coll)
	{
		//Jeśli to gracz, przejdź w tryb pościgu
		if (coll.tag == "Player")
		{			
			m_state = State.FOLLOW;
			m_lastSeenPosition = m_player.transform.position;
			m_character.SetTargetRotation(m_lastSeenPosition);
			m_alarmTimer = 0.0f;
		}

		//Jeśli to ognisty pocisk, odjęcie punktów zdrowia
		if (coll.tag == "Fireball")
		{
			m_health -= 40.0f;
			//Jeśli punkty zdrowia się skończyły, usunięcie obiektu
			if (m_health <= 0.0f)
				Destroy(this.gameObject);
		}

		//Jeśli to lodowy pocisk, zmniejszenie prędkości poruszania
		if (coll.tag == "Iceball")
		{
			m_freezeTimerOn = true;
			m_speedMultiplayer = m_freezeSpeedMultiplayer;
		}

		//Jeśli to elektryczny pocisk, powrót do trybu patrolu
		if (coll.tag == "Lightningball")
		{
			m_state = State.PATROL;
			m_shockTimerOn = true;
			m_shocked = true;
		}

		//Jeśli to portal, ustawienie flagi
		if (coll.tag == "Portal")
		{
			m_hitPortal = true;
		}
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update()
	{
		m_camera.transform.rotation = this.transform.rotation;

		CheckForPlayer(); //Sprawdzenie, czy SI widzi obiekt gracza
		HandleTimers(); //Obsługa liczników
	}

	/// <summary>
	/// Sprawdza, czy postać sterowana przez SI widzi obiekt gracza i w zależności od tego wykonuje odpowiednie operacje.
	/// </summary>
	void CheckForPlayer()
	{
		//Jeżeli postać nie jest w szoku
		if (!m_shocked)
		{
			m_planes = GeometryUtility.CalculateFrustumPlanes (m_camera); //Pobranie figury pola widzenia SI
			//Jeśli gracz mieści się w polu widzenia lub znajduje się bardzo blisko postaci
			if (GeometryUtility.TestPlanesAABB (m_planes, m_playerColl.bounds) || Vector3.Distance(this.transform.position, m_player.transform.position) <= 2.0f)
			{
				//Jeżeli między postacią SI oraz graczem nie ma przeszkody
				if (CanSeePlayer ())
				{
					m_state = State.FOLLOW; //Ścigaj gracza
					m_lastSeenPosition = m_player.transform.position;
					m_isOnCooldown = true;
				} 
				else
				{
					//Jeśli SI jest w trakcie pościgu i nie widzi gracza, przechodzi w stan alarmu
					if (m_state == State.FOLLOW)
					{
						m_state = State.ALARMED;
						m_hitPortal = false;
					}

				}
			}
			else 
			{
				//Jeśli SI jest w trakcie pościgu i nie widzi gracza, przechodzi w stan alarmu
				if (m_state == State.FOLLOW)
				{
					m_state = State.ALARMED;
					m_hitPortal = false;
				}			
			}
		}
	}

	/// <summary>
	/// Sprawdza, czy SI widzi postać gracza.
	/// </summary>
	/// <returns><c>true</c> jeśli SI widzi gracza; w przeciwnym razie, <c>false</c>.</returns>
	bool CanSeePlayer()
	{
		RaycastHit hit;
		if (Physics.Raycast (m_camera.transform.position, m_player.transform.position - m_camera.transform.position, out hit, 50.0f))
		{
			if (hit.collider.gameObject.tag == "Player") 
			{
				return true;
			}
			return false;
		}
		return true;
	}

	/// <summary>
	/// Przewiduje miejsce zderzenia postaci oraz pocisku.
	/// </summary>
	/// <returns><c>true</c>, jeśli pocisk może się zderzyć z celem, <c>false</c> w przeciwnym wypadku.</returns>
	/// <param name="predictedPos">Przewidziane miejsce kolizji.</param>
	bool PredictTargetPos(out Vector3 predictedPos)
	{
		Vector3 targetOffset = m_player.transform.position - this.transform.position;
		Rigidbody playerRB = m_player.GetComponent<Rigidbody> ();
		float targetMoveAngle = Vector3.Angle (-targetOffset, playerRB.velocity) * Mathf.Deg2Rad;
		if (playerRB.velocity.magnitude == 0.0f)
		{
			predictedPos = m_player.transform.position;
			return true;
		}
		else if (playerRB.velocity.magnitude > m_bulletVelocity && Mathf.Sin(targetMoveAngle) / m_bulletVelocity > Mathf.Cos(targetMoveAngle) / playerRB.velocity.magnitude)
		{
			predictedPos = m_player.transform.position;
			return false;
		}
		else
		{
			float shootAngle = Mathf.Asin (Mathf.Sin (targetMoveAngle) * playerRB.velocity.magnitude / m_bulletVelocity);
			predictedPos = m_player.transform.position + playerRB.velocity * targetOffset.magnitude / Mathf.Sin (Mathf.PI - targetMoveAngle - shootAngle) * Mathf.Sin (shootAngle) / playerRB.velocity.magnitude;
			return true;
		}
	}

	/// <summary>
	/// Obsługa liczników czasu.
	/// </summary>
	void HandleTimers()
	{
		if (m_alarmTimerOn)
			m_alarmTimer += Time.deltaTime;

		if (m_freezeTimerOn)
		{
			m_freezeTimer += Time.deltaTime;
			if (m_freezeTimer >= m_freezeLength)
			{
				m_freezeTimerOn = false;
				m_speedMultiplayer = 1.0f;
				m_freezeTimer = 0.0f;
			}

		}

		if (m_shockTimerOn)
		{
			m_shockTimer += Time.deltaTime;
			if (m_shockTimer >= m_shockLength)
			{
				m_shockTimerOn = false;
				m_shocked = false;
				m_shockTimer = 0.0f;
			}

		}

		if (m_patrolWaitTimerOn)
			m_patrolWaitTimer += Time.deltaTime;

		if (m_isOnCooldown)
		{
			m_shootCooldownTimer += Time.deltaTime;
			if (m_shootCooldownTimer >= m_shootingCooldown)
			{
				m_shootCooldownTimer = 0.0f;
				m_isOnCooldown = false;
			}
		}
	}

	/// <summary>
	/// Wyznaczenie punktu, w który powinna celować broń postaci.
	/// </summary>
	void HandleGun()
	{
		GunController gunController = m_gun.GetComponent<GunController>();
		if (gunController)
		{
			if (m_state == State.FOLLOW)
			{
				Vector3 posToShoot; //Zmienna, do której zostanie przypisany punkt, w który powinna celować broń
				bool canShoot = PredictTargetPos (out posToShoot);
				gunController.AimTo(posToShoot, false);
				//Jeśli można, strzał
				if (canShoot && !m_isOnCooldown)
				{
					gunController.Shoot (5.0f, m_bulletVelocity);
					m_isOnCooldown = true;
				}
			} 
			else 
			{
				gunController.SetTargetRotation(this.transform.forward);
			}
		}
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		HandleGun();
	}
}