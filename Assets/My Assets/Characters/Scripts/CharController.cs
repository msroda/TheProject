﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))] //Reprezentuje obiekt w symulacji silnika fizyki
[RequireComponent(typeof(Collider))] //Do wykrywania kolizji

/// <summary>
/// Kontroler postaci.
/// </summary>
public class CharController : MonoBehaviour {

	/// <summary>
	/// Siła skoku.
	/// </summary>
	public float m_JumpPower = 12f;
	/// <summary>
	/// Mnożnik oddziaływania siły grawitacji na postać.
	/// </summary>
	[Range(1f, 4f)]public float m_GravityMultiplier = 2f;
	/// <summary>
	/// Prędkość poruszania się.
	/// </summary>
	public float m_MoveSpeedMultiplier = 1f;
	/// <summary>
	/// Prędkość podczas biegu po ścianie w górę.
	/// </summary>
	public float m_parkourSpeedVertical = 6f;
	/// <summary>
	/// Prędkość podczas biegu wzdłuż ściany.
	/// </summary>
	public float m_parkourSpeedHorizontal = 1f;
	/// <summary>
	/// Dokładność podczas sprawdzania, czy postać stoi na ziemi.
	/// </summary>
	public float m_GroundCheckDistance = 0.1f;

	Rigidbody m_Rigidbody; //Reprezentuje obiekt w symulacji silnika fizyki
	[HideInInspector] public bool m_IsGrounded; //Czy postać stoi na ziemi
	float m_OrigGroundCheckDistance; //Dokładność podczas sprawdzania, czy postać stoi na ziemi
	Vector3 m_GroundNormal; //Normalna powierzchni, na której stoi postać
	Quaternion m_targetRotation; //Docelowa rotacja postaci
	private bool m_wallSide; //Z której strony znajduje się ściana, po której biegnie postać
	private bool m_isWallrunningUp = false; //Czy postać biegnie po ścianie w górę
	private bool m_isWallrunningSide = false; //Czy postać biegnie wzdłuż ściany
	private Vector3[] m_wallNormals; //Normalne ścian, z którymi koliduje postać z różnych stron
	private Vector3 m_lastDesiredMove; //Ostatnia prędkość zadana przez gracza lub SI
	private Vector3 m_rawMove; //Niezrzutowany wektor prędkości, zadany przez gracza lub SI
	private bool m_hasWallJumped = false; //Czy postać skoczyła od ściany

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start () 
	{
		m_Rigidbody = GetComponent<Rigidbody>();

		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
		m_OrigGroundCheckDistance = m_GroundCheckDistance;
		m_wallNormals = new Vector3[4];
	}

	/// <summary>
	/// Obsługa ruchu postaci.
	/// </summary>
	/// <param name="move">Docelowy wektor prędkości.</param>
	/// <param name="jump">Czy powinien nastąpić skok.</param>
	/// <param name="parkour">Czy powinien nastąpić bieg po śćianie.</param>
	public void Move(Vector3 move, bool jump, bool parkour)
	{
		if (m_Rigidbody)
		{	
			move = transform.InverseTransformDirection (move); //Przekształcenie prędkości na lokalną dla obiektu
			m_rawMove = transform.rotation * move * m_MoveSpeedMultiplier;

			CheckGroundStatus (); //Sprawdzenie, czy postać znajduje się na ziemi
			move = transform.rotation * Vector3.ProjectOnPlane (move, m_GroundNormal); //Zrzutowanie wektora prędkości na powierzchnię poruszania się

			//Jeśli postać nie biegnie po ścianie, zapisanie jej ostatniego ruchu
			if (!m_isWallrunningSide && !m_isWallrunningUp)
				m_lastDesiredMove = move;

			//Wywołanie odpowiedniej funkcji do dalszej obsługi ruchu, w zależności od tego, czy postać stoi na ziemi, czy nie
			if (m_IsGrounded) {
				HandleGroundedMovement (jump, parkour, move);
			} else {
				HandleAirborneMovement (jump, parkour);
			}
			//Wyczyszczenie wektora normalnych
			m_wallNormals = new Vector3[4];
		}
	}

	/// <summary>
	/// Obsługa ruchu postaci w powietrzu.
	/// </summary>
	/// <param name="jump">Czy powinien nastąpić skok.</param>
	/// <param name="parkour">Czy powinien nastąpić bieg po śćianie.</param>
	void HandleAirborneMovement(bool jump, bool parkour)
	{
		//Jeśli obiekt koliduje ze ścianą i ma nastąpić skok - skocz od ściany
		if (jump && GetWallJumpDirection().magnitude > 0.1f)
				WallJump();
		//Jeśli postać biegnie po ścianie w górę i bieg ten ma być kontynuowany
		else if (m_isWallrunningUp && parkour)
		{
			//Jeśli postać nie posiada już prędkości pionowej w górę, przerwij bieg
			if (m_Rigidbody.velocity.y <= 0.0f)
				m_isWallrunningUp = false;
			//Jeśli postać nie ma już przed sobą ściany, przerwij bieg
			else if (m_wallNormals [0].magnitude < 0.1f) 
			{
				m_Rigidbody.velocity = new Vector3 (m_wallNormals [0].normalized.x * 0.1f, m_Rigidbody.velocity.y, m_wallNormals [0].normalized.z * 0.1f);
				m_isWallrunningUp = false;
			} 
			//Kontynuuj bieg
			else 
			{
				SetTargetRotation (-new Vector3 (m_wallNormals [0].x, 0.0f, m_wallNormals [0].z));
				m_Rigidbody.velocity -= m_wallNormals [0].normalized * 0.1f; //Dodanie prędkości skierowanej w stronę ściany, by postać nie straciła z nią kontaktu
			}

		} 
		//Jeśli postać biegnie po ścianie wzdłuż niej i bieg ten ma być kontynuowany
		else if (m_isWallrunningSide && parkour) 
		{
			//Jeśli prędkość opadania jest za duża lub postać nie koliduje już ze ścianą, przerwij bieg
			if (m_Rigidbody.velocity.y <= -m_JumpPower
				|| (m_wallSide && m_wallNormals[1].magnitude < 0.1f)
				|| (!m_wallSide && m_wallNormals[2].magnitude < 0.1f))
				m_isWallrunningSide = false;
			//W przeciwnym wypadku kontynuuj bieg
			else
			{
				Vector3 tempVel;
				//Projekcja prędkości postaci na ścianę, w zależności od tego, po której stronie się ona znajduje
				if(m_wallSide)					
					tempVel = Vector3.ProjectOnPlane(m_Rigidbody.velocity, m_wallNormals[1]);
				else
					tempVel = Vector3.ProjectOnPlane(m_Rigidbody.velocity, m_wallNormals[2]);
				//Wyznaczenie nowej prędkości względem ściany
				tempVel = new Vector3 (tempVel.x, 0.0f, tempVel.z).normalized * m_MoveSpeedMultiplier * 0.7f;
				m_Rigidbody.velocity = new Vector3 (tempVel.x, m_Rigidbody.velocity.y, tempVel.z);
				m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
				SetTargetRotation(tempVel);
			}
		}
		//W przeciwnym razie, kontynuuj spadanie
		else 
		{
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce (extraGravityForce);
			m_isWallrunningUp = false;
			m_isWallrunningSide = false;
			//Jeśli postać nie skoczyła od ściany, pozwól graczowi na zmianę kierunku poruszania się podczas lotu
			if (m_rawMove.magnitude > 0.1f && !m_hasWallJumped)
			{
				Vector2 currentVelocity = new Vector2 (m_Rigidbody.velocity.x, m_Rigidbody.velocity.z);
				Vector2 desiredMove = Vector2.Lerp (currentVelocity, new Vector2 (m_rawMove.x, m_rawMove.z), 2.0f * Time.deltaTime);
				if (desiredMove.magnitude > currentVelocity.magnitude)
					desiredMove *= (currentVelocity.magnitude / desiredMove.magnitude);
				m_Rigidbody.velocity = new Vector3 (desiredMove.x, m_Rigidbody.velocity.y, desiredMove.y);
			}
			m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
		}
	}

	/// <summary>
	/// Obsługa ruchu postaci po ziemi.
	/// </summary>
	/// <param name="jump">Czy powinien nastąpić skok.</param>
	/// <param name="parkour">Czy powinien nastąpić bieg po śćianie.</param>
	/// <param name="move">Docelowy wektor prędkości.</param>
	void HandleGroundedMovement(bool jump, bool parkour, Vector3 move)
	{
		//Jeśli ma nastąpić skok, skocz
		if (jump)
		{
			m_Rigidbody.velocity = new Vector3 (m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z); //Dodanie postaci prędkości w górę
			m_hasWallJumped = false;
			m_IsGrounded = false;
			m_GroundCheckDistance = 0.1f;
		}

		//Jeśli ma nastąpić bieg po ścianie i postać ma przed sobą ścianę, rozpocznij bieg po ścianie
		else if (parkour && m_wallNormals[0].magnitude > 0.1f)
		{
			m_Rigidbody.velocity = new Vector3 (m_Rigidbody.velocity.x, m_parkourSpeedVertical, m_Rigidbody.velocity.z) - m_wallNormals[0].normalized * 0.1f;
			m_IsGrounded = false;
			m_isWallrunningUp = true;
			m_GroundCheckDistance = 0.1f;
			SetTargetRotation(this.transform.rotation * -new Vector3(m_wallNormals[0].x, 0.0f, m_wallNormals[0].z));
		}
		//Jeśli ma nastąpić bieg po ścianie i postać ma po lewej ścianę, rozpocznij bieg po ścianie
		else if (parkour && m_wallNormals[1].magnitude > 0.1f)
		{
			Vector3 tempVel = Vector3.ProjectOnPlane(m_Rigidbody.velocity, m_wallNormals[1]);
			tempVel = new Vector3 (tempVel.x, 0.0f, tempVel.z).normalized * m_parkourSpeedHorizontal;
			m_Rigidbody.velocity = new Vector3 (tempVel.x, m_parkourSpeedVertical * 0.85f, tempVel.z);
			m_IsGrounded = false;
			m_isWallrunningSide = true;
			m_GroundCheckDistance = 0.1f;
			SetTargetRotation(this.transform.rotation * tempVel);
			m_wallSide = true;
		}
		//Jeśli ma nastąpić bieg po ścianie i postać ma po prawej ścianę, rozpocznij bieg po ścianie
		else if (parkour && m_wallNormals[2].magnitude > 0.1f)
		{
			Vector3 tempVel = Vector3.ProjectOnPlane(m_Rigidbody.velocity, m_wallNormals[2]);
			tempVel = new Vector3 (tempVel.x, 0.0f, tempVel.z).normalized * m_parkourSpeedHorizontal;
			m_Rigidbody.velocity = new Vector3 (tempVel.x, m_parkourSpeedVertical * 0.85f, tempVel.z);
			m_IsGrounded = false;
			m_isWallrunningSide = true;
			m_GroundCheckDistance = 0.1f;
			SetTargetRotation(this.transform.rotation * tempVel);
			m_wallSide = false;
		}
		//W przewicnym razie poruszaj postacią po ziemi
		else 
		{
			m_Rigidbody.velocity = move * m_MoveSpeedMultiplier;
		}
	}

	/// <summary>
	/// Obsługa skoku od ściany.
	/// </summary>
	void WallJump()
	{
		m_Rigidbody.velocity = GetWallJumpDirection() * 4;
		m_IsGrounded = false;
		m_GroundCheckDistance = 0.1f;
		m_isWallrunningUp = false;
		m_isWallrunningSide = false;
		m_hasWallJumped = true;
	}

	/// <summary>
	/// Sprawdza, czy postać znajduje się na ziemi.
	/// </summary>
	void CheckGroundStatus()
	{
		RaycastHit hitInfo;
		if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
		{
			m_GroundNormal = hitInfo.normal;
			m_IsGrounded = true;
		}
		else
		{
			m_IsGrounded = false;
			m_GroundNormal = Vector3.up;
		}
	}

	/// <summary>
	/// Sprawdza, czy powierzchnia o zadanej normalnej jest ścianą i zapisuje ją w odpowiednim miejscu w tablicy.
	/// </summary>
	/// <param name="newNormal">Normalna badanej powierzchni.</param>
	void AddWallNormal (Vector3 newNormal)
	{
		Vector3 vecFromVelocity = new Vector2(m_lastDesiredMove.x, m_lastDesiredMove.z); //Zrzutowanie wektorów do przestrzeni dwuwymiarowej.
		Vector3 vecFromVelocityRotated = new Vector2(m_lastDesiredMove.z, m_lastDesiredMove.x);
		Vector3 vecFromWallNormal = new Vector2(newNormal.x, newNormal.z);

		float angle = Vector2.Angle (vecFromVelocity, vecFromWallNormal);
		float angle2 = Vector2.Angle (vecFromVelocityRotated, vecFromWallNormal);

		if (angle >= 135)
		{
			m_wallNormals[0] = newNormal.normalized;
		}			
		else if (angle >= 45)
		{
			if(angle2 >= 90)
			{
				m_wallNormals[1] = newNormal;
			}
			else
			{
				m_wallNormals[2] = newNormal;
			}
		}
		else
			m_wallNormals[3] = newNormal;
	}

	/// <summary>
	/// Wyznacza prędkość postaci po wykonaniu skoku od ściany.
	/// </summary>
	/// <returns>Docelową prędkość postaci po wykonaniu skoku od ściany.</returns>
	Vector3 GetWallJumpDirection()
	{
		Vector3 sum = Vector3.zero;

		//Wyznaczenie wypadkowego wektora na podstawie 4 powierzchni, z którymi styka się postać
		for (int i = 0; i < 4; i++)
			if (m_wallNormals[i].magnitude > 0.1f)
				sum += m_wallNormals [i];

		if (sum.magnitude < 0.1f)
			return Vector3.zero;

		return new Vector3 (sum.normalized.x, m_JumpPower * 0.2f, sum.normalized.z);
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję.
	/// </summary>
	/// <param name="collision">Informacje na temat kolizji.</param>
	void OnCollisionEnter(Collision collision)
	{
		foreach (ContactPoint point in collision.contacts) 
		{
			if (Mathf.Abs(point.normal.y) < 0.1f)
			{
				AddWallNormal (point.normal);
			}
		}
	}

	/// <summary>
	/// Obsługa zdarzenia pozostawania obiektu w kolizji.
	/// </summary>
	/// <param name="collision">Informacje na temat kolizji.</param>
	void OnCollisionStay(Collision collision)
	{
		foreach (ContactPoint point in collision.contacts) 
		{
			if (Mathf.Abs(point.normal.y) < 0.1f)
			{
				AddWallNormal (point.normal);
			}
		}			
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		transform.rotation = Quaternion.RotateTowards(transform.rotation, m_targetRotation, 360 * Time.fixedDeltaTime);
	}

	/// <summary>
	/// Ustala docelową rotację postaci.
	/// </summary>
	/// <param name="targetPos">Punkt, w kierunku którego powinien być skierowany obiekt.</param>
	public void SetTargetRotation(Vector3 targetPos)
	{
		targetPos.y = 0.0f;
		if (targetPos.magnitude > 0.1)
			m_targetRotation = Quaternion.LookRotation (targetPos);
	}
}
