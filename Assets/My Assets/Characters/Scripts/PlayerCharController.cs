﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof (CharController))] //Postać do poruszania

/// <summary>
/// Kontroler postaci (Gracza).
/// </summary>
public class PlayerCharController : MonoBehaviour {

	private CharController m_character; //Postać do poruszania
	private Transform m_cam; //Kamera
	private Vector3 m_camForward; //Wektor okreśłający kierunek kamery
	private Vector3 m_move; //Docelowy wektor prędkości postaci
	private bool m_jump; //Czy ma nastąpić skok
	private bool m_parkour; //Czy ma nastąpić bieg po ścianie

	/// <summary>
	/// Obiekt obsługujący pasek życia postaci.
	/// </summary>
	public GameObject m_playerStatusBar;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		if (Camera.main != null)
		{
			m_cam = Camera.main.transform;
		}
		else
		{
			Debug.LogWarning("No camera found!");
		}
			
		m_character = GetComponent<CharController>();
	}

	/// <summary>
	/// Pobiera informacje od gracza z klawiatury.
	/// </summary>
	void GetUserKeyboardInput()
	{
		if (!m_jump)
		{
			m_jump = CrossPlatformInputManager.GetButtonDown("Jump");		
		}

		m_parkour = Input.GetKey(KeyCode.LeftShift);
	}

	/// <summary>
	/// Pobiera informacje od gracza z myszy.
	/// </summary>
	void GetUserMouseInput()
	{
		float h = CrossPlatformInputManager.GetAxis("Horizontal");
		float v = CrossPlatformInputManager.GetAxis("Vertical");

		if (m_cam != null)
		{
			//Określenie docelowego wektora prędkości względem orientacji kamery
			m_camForward = Vector3.Scale(m_cam.forward, new Vector3(1, 0, 1)).normalized;
			m_move = v*m_camForward + h*m_cam.right;
		}
		else
		{
			m_move = v*Vector3.forward + h*Vector3.right;
		}

		//Jeśli gracz celuje
		if (Input.GetMouseButton(1))
		{
			m_move *= 0.5f;
			m_character.SetTargetRotation(m_cam.forward);
		}
		else
		{
			m_character.SetTargetRotation(m_move);
		}
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	private void Update()
	{
		GetUserKeyboardInput ();
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	private void FixedUpdate()
	{
		GetUserMouseInput();
			
		m_character.Move(m_move, m_jump, m_parkour);
		m_jump = false;
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerEnter(Collider coll)
	{
		if (coll.tag == "EnemyBullet")
		{
			PlayerBar pb = m_playerStatusBar.GetComponent<PlayerBar> ();
			if (pb)
				pb.OnDamage (20.0f);
		}
	}
}
