﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Klasa obsługująca menu wyświetlane po przejściu gry.
/// </summary>
public class FinishMenu : MonoBehaviour {

	/// <summary>
	/// Nazwa pliku sceny z pierwszym poziomem.
	/// </summary>
	public string m_firstLevel;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start()
	{
		Time.timeScale = 1.0f;
		Cursor.visible = true;
	}

	/// <summary>
	/// Obsługa przycisku wyjścia z aplikacji.
	/// </summary>
	public void OnExitApp()
	{
		Application.Quit ();
	}

	/// <summary>
	/// Obsługa przycisku rozpoczęcia nowej gry.
	/// </summary>
	public void OnNewGame()
	{
		SceneManager.LoadScene (m_firstLevel);
	}
}

