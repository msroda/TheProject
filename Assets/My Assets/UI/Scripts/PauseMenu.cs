﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Klasa obsługująca menu wyświetlane podczas pauzy w grze.
/// </summary>
public class PauseMenu : MonoBehaviour {

	/// <summary>
	/// Obiekt menu do wyświetlenia.
	/// </summary>
	public GameObject menu;
	private bool menuOn; //Czy menu jest aktualnie wyświetlone

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		menuOn = false;
		menu.SetActive(menuOn);
		Cursor.visible = false;
		Time.timeScale = 1.0f;
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			menuOn = !menuOn;
			menu.SetActive(menuOn);

			//Jeśli menu ma zostać wyświetlone, zatrzymaj też upływ czasu w grze
			if (menuOn)
			{
				Cursor.visible = true;
				Time.timeScale = 0.0f;
			}
			else
			{
				Cursor.visible = false;
				Time.timeScale = 1.0f;
			}
		}
	}

	public void OnExitButton()
	{
		Application.Quit ();
	}

	public void OnResumeButton()
	{
		Debug.Log ("A");
		menuOn = false;
		menu.SetActive(menuOn);
		Cursor.visible = false;
		Time.timeScale = 1.0f;
	}

	public void OnRestartButton()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
