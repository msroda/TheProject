﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Klasa obsługująca pasek życia postaci oraz interfejs użytkownika.
/// </summary>
public class PlayerBar : MonoBehaviour {

	/// <summary>
	/// Obiekt menu pauzy.
	/// </summary>
	public GameObject m_pauseMenu;
	/// <summary>
	/// Obiekt menu końca gry.
	/// </summary>
	public GameObject m_GameOverMenu;

	/// <summary>
	/// Obrazek paska życia.
	/// </summary>
	public Image m_healthBar;
	/// <summary>
	/// Ikony typów pocisków
	/// </summary>
	public Image[] m_gunIcons;
	private float m_imageSize; //Szerokość obrazka paska życia
	private float m_health; //Aktualny stan punktów życia postaci
	private int m_currentIcon; //Aktualnie wyświetlana ikona typu pocisków

	/// <summary>
	/// Czas, po którym punkty życia zaczną się regenerować.
	/// </summary>
	public float m_healingCooldown = 1.0f;
	private float m_cooldownTimer = 0.0f; //Licznik czasu, po którym punkty życia zaczną się regenerować
	private bool m_cooldown = false; //Czy punkty zdrowia mogą aktualnie zostać regenerowane
	/// <summary>
	/// Prędkość regeneracji punktów życia.
	/// </summary>
	public float m_healingSpeed = 0.2f;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		m_health = 100.0f;
		m_currentIcon = 0;
		for(int i = 0; i < m_gunIcons.Length; i++)
			m_gunIcons [i].enabled = false;
		m_gunIcons [m_currentIcon].enabled = true;
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update ()
	{
		//Odliczanie czasu do regeneracji
		if (m_cooldown)
		{
			m_cooldownTimer += Time.deltaTime;
			if (m_cooldownTimer >= m_healingCooldown) {
				m_cooldownTimer = 0.0f;
				m_cooldown = false;
			}
		}
		//Regeneracja punktów życia
		else
		{
			m_health = Mathf.Max(Mathf.Min (m_health + Time.deltaTime * m_healingSpeed, 100.0f), 0.0f);
		}
		m_healthBar.rectTransform.localScale = new Vector3(m_health / 100.0f, 1.0f, 1.0f); //Zmiana rozmiaru paska życia

		//Zmiana wyświetlanej ikony typu pocisków
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			m_gunIcons [m_currentIcon].enabled = false;
			m_currentIcon = 0;
			m_gunIcons [m_currentIcon].enabled = true;
		}
		if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			m_gunIcons [m_currentIcon].enabled = false;
			m_currentIcon = 1;
			m_gunIcons [m_currentIcon].enabled = true;
		}
		if(Input.GetKeyDown(KeyCode.Alpha3))
		{
			m_gunIcons [m_currentIcon].enabled = false;
			m_currentIcon = 2;
			m_gunIcons [m_currentIcon].enabled = true;
		}
		if(Input.GetKeyDown(KeyCode.Alpha4))
		{
			m_gunIcons [m_currentIcon].enabled = false;
			m_currentIcon = 3;
			m_gunIcons [m_currentIcon].enabled = true;
		}
		if(Input.GetKeyDown(KeyCode.Alpha5))
		{
			m_gunIcons [m_currentIcon].enabled = false;
			m_currentIcon = 4;
			m_gunIcons [m_currentIcon].enabled = true;
		}
	}

	/// <summary>
	/// Zmniejsza ilość punktów życia.
	/// </summary>
	/// <param name="damage">Ilość punktów do odjęcia.</param>
	public void OnDamage(float damage)
	{
		m_health -= damage * 0.5f;
		//Jeśli gracz umarł
		if (m_health <= 0.0f)
		{
			m_health = 0.0f;
			m_pauseMenu.SetActive (false); //Wyłączenie menu pauzy
			GameOverMenu gomenu = m_GameOverMenu.GetComponent<GameOverMenu>();
			if (gomenu) //Wywołanie obsługi końca gry
				gomenu.OnGameOver();
		}
		m_cooldown = true;
		m_cooldownTimer = 0.0f;
	}
}
