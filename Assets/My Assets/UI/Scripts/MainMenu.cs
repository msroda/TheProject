﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Klasa obsługująca menu główne gry.
/// </summary>
public class MainMenu : MonoBehaviour {

	/// <summary>
	/// Strona główna menu.
	/// </summary>
	public GameObject mainPage;
	/// <summary>
	/// Podstrony menu.
	/// </summary>
	public GameObject[] subMenus;
	/// <summary>
	/// Nazwa pliku sceny z pierwszym poziomem.
	/// </summary>
	public string m_firstLevel;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start()
	{
		Time.timeScale = 1.0f;
		Cursor.visible = true;
	}

	/// <summary>
	/// Obsługa przycisku przejścia do podstrony menu.
	/// </summary>
	/// <param name="index">Indeks strony.</param>
	public void OnExploreMenu(int index)
	{
		subMenus[index].SetActive (true);
		mainPage.SetActive (false);
	}

	/// <summary>
	/// Obsługa przycisku powrótu do strony głównej menu.
	/// </summary>
	public void OnBack()
	{
		foreach (GameObject obj in subMenus)
			obj.SetActive (false);
		mainPage.SetActive (true);
	}

	/// <summary>
	/// Obsługa przycisku wyjścia z aplikacji.
	/// </summary>
	public void OnExitApp()
	{
		Application.Quit ();
	}

	/// <summary>
	/// Obsługa przycisku rozpoczęcia nowej gry.
	/// </summary>
	public void OnNewGame()
	{
		SceneManager.LoadScene (m_firstLevel);
	}
}
