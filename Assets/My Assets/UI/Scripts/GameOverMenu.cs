﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Klasa obsługująca menu wyświetlane, gdy gracz przegra.
/// </summary>
public class GameOverMenu : MonoBehaviour {

	/// <summary>
	/// Obiekt menu do wyświetlenia;
	/// </summary>
	public GameObject menu;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		menu.SetActive(false);
	}

	/// <summary>
	/// Obsługa przycisku wyjścia.
	/// </summary>
	public void OnExitButton()
	{
		Application.Quit ();
	}

	/// <summary>
	/// Obsługa przycisku restartu gry.
	/// </summary>
	public void OnRestartButton()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	/// <summary>
	/// Obsługa zdarzenia przegrania gry.
	/// </summary>
	public void OnGameOver()
	{
		Cursor.visible = true;
		Time.timeScale = 0.0f;
		menu.SetActive(true);
	}


}
