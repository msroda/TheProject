﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kontroler ruchomych platform.
/// </summary>
public class MovingPlatformController : MonoBehaviour {

	/// <summary>
	/// Sposób wyboru ścieżki poruszania platformy.
	/// </summary>
	public enum RouteType
	{
		/// <summary>
		/// W pętli.
		/// </summary>
		LOOP,
		/// <summary>
		/// Tam i z powrotem.
		/// </summary>
		BACKANDFORTH
	}

	/// <summary>
	/// Sposób wyboru ścieżki poruszania platformy.
	/// </summary>
	public RouteType m_routeType;
	/// <summary>
	/// Obiekty, na podstawie których zostaną określone punkty, pomiędzy którymi porusza się platforma.
	/// </summary>
	public GameObject[] m_waypoints;
	private Vector3[] m_transformedWaypoints; //Punkty, pomiędzy którymi porusza się platforma
	private int m_waypointIndex; //Indeks wskazujący obecny punkt docelowy platformy
	/// <summary>
	/// Prędkość poruszania platformy.
	/// </summary>
	public float m_speed;
	/// <summary>
	/// Czas czekania platformy w punkcie.
	/// </summary>
	public float m_stopTime;
	private List<GameObject> m_objectsToMove; //Lista obiektów, które powinny być przemieszczone z platformą
	private bool m_direction; //Kierunek poruszania platformy (w trybie "tam i z powrotem")
	private Vector3 m_currentVelocity; //Obecna prędkość platformy
	private float m_stopTimer; //Licznik czasu oczekiwnia platformy w punkcie
	private bool m_stop; //Czy platforma powinna stać

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start()
	{
		m_transformedWaypoints = new Vector3[m_waypoints.Length + 1];
		m_transformedWaypoints [0] = this.transform.position;
		for (int i = 1; i <= m_waypoints.Length; i++)
		{
			m_transformedWaypoints [i] = m_waypoints [i - 1].transform.position;
			Destroy(m_waypoints [i - 1]);
		}
		m_waypointIndex = 1;
		m_objectsToMove = new List<GameObject>();
		m_direction = true;
		m_stop = true;
		m_stopTimer = 0.0f;
	}

	/// <summary>
	/// Przemieszcza platformę oraz znajdujące się na niej i jej drodze obiekty.
	/// </summary>
	void Move()
	{
		float distance = Vector3.Distance (m_transformedWaypoints [m_waypointIndex], this.transform.position);

		//Obliczenie przemieszczenia platformy między akutalizacjami silnika fizyki
		Vector3 displacement = (m_transformedWaypoints [m_waypointIndex] - this.transform.position).normalized * m_speed * Time.fixedDeltaTime;

		if (Mathf.Abs (displacement.magnitude) + 0.01f >= Mathf.Abs (distance))
		{
			displacement = displacement.normalized * Vector3.Distance (m_transformedWaypoints [m_waypointIndex], this.transform.position);
			this.transform.position = m_transformedWaypoints [m_waypointIndex];
			NextIndex ();
		}
		else
			this.transform.position += displacement;

		m_currentVelocity = displacement;

		//Przesunięcie obiektów na platformie i na jej drodze
		foreach (GameObject gobj in m_objectsToMove)
		{
			gobj.transform.position += displacement;
		}
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update()
	{
		HandleTimers ();
	}

	/// <summary>
	/// Obsługa liczników czasu.
	/// </summary>
	void HandleTimers()
	{
		if (m_stop)
		{
			m_stopTimer += Time.deltaTime;
			if (m_stopTimer >= m_stopTime)
			{
				m_stop = false;
				m_stopTimer = 0.0f;
			}
		}
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		if (!m_stop)
			Move ();
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję.
	/// </summary>
	/// <param name="collision">Informacje na temat kolizji.</param>
	void OnCollisionEnter(Collision col)
	{
		if (ShouldMoveObject (col.contacts))
		{
				m_objectsToMove.Add (col.gameObject);
		}
	}

	/// <summary>
	/// Obsługa zdarzenia pozostawania obiektu w kolizji.
	/// </summary>
	/// <param name="collision">Informacje na temat kolizji.</param>
	void OnCollisionStay(Collision col)
	{
		if (ShouldMoveObject (col.contacts))
		{
			if(!m_objectsToMove.Contains(col.gameObject))
				m_objectsToMove.Add (col.gameObject);
		}
		else
		{
			if(m_objectsToMove.Contains(col.gameObject))
				m_objectsToMove.Remove(col.gameObject);
		}
	}

	/// <summary>
	/// Obsługa zdarzenia wyjścia obiektu z kolizji.
	/// </summary>
	/// <param name="collision">Informacje na temat kolizji.</param>
	void OnCollisionExit(Collision col)
	{
		if(m_objectsToMove.Contains(col.gameObject))
			m_objectsToMove.Remove(col.gameObject);
	}

	/// <summary>
	/// Wyznaczenie indeksu kolejnego punktu docelowegou.
	/// </summary>
	void NextIndex()
	{
		switch (m_routeType)
		{
		case (RouteType.LOOP):
			m_waypointIndex++;
			if (m_waypointIndex >= m_transformedWaypoints.Length)
				m_waypointIndex = 0;
			break;
		case (RouteType.BACKANDFORTH):
			if (m_direction)
			{
				
				if (m_waypointIndex >= m_transformedWaypoints.Length - 1)
				{
					m_waypointIndex = m_transformedWaypoints.Length - 2;
					m_direction = false;
				}
				else
					m_waypointIndex++;
				break;
			} 
			else 
			{
				if (m_waypointIndex <= 0)
				{
					m_waypointIndex = 1;
					m_direction = true;
				}
				else
					m_waypointIndex--;
				break;
			}

		}
		m_stop = true;
	}

	/// <summary>
	/// Sprawdza, czy dany obiekt powinien zostać przemieszczony z platformą.
	/// </summary>
	/// <returns><c>true</c>, jeśli obiekt powinien poruszać się z platformą, <c>false</c> jeśli nie.</returns>
	/// <param name="points">Points.</param>
	bool ShouldMoveObject(ContactPoint[] points)
	{
		foreach (ContactPoint point in points) 
		{
			if (point.normal.y < -0.0f || Vector3.Dot(m_currentVelocity.normalized, point.normal) < 0.0f)
			{
				return true;
			}
		}
		return false;
	}
}
