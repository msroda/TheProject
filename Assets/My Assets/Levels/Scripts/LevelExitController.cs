﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider))] //Do wykrywania kolizji

/// <summary>
/// Kontroler wyjścia z poziomu.
/// </summary>
public class LevelExitController : MonoBehaviour {

	/// <summary>
	/// Nazwa pliku sceny z kolejnym poziomem.
	/// </summary>
	public string m_nextStage;
	private Camera m_cam; //Główna kamera

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		m_cam = Camera.main;
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update ()
	{
		transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(transform.right), 180 * Time.deltaTime);
	}

	/// <summary>
	/// Obsługa zdarzenia wejścia obiektu w kolizję z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerEnter(Collider coll)
	{

		if (coll.tag == "Player")
		{
			Time.timeScale = 0.5f;
			GameObject player = coll.gameObject;
			player.transform.position = new Vector3(this.transform.position.x, player.transform.position.y, this.transform.position.z);
			Rigidbody playerRB = player.GetComponent<Rigidbody>();
			if (playerRB)
			{
				playerRB.useGravity = false;
				playerRB.velocity = Vector3.zero;
			}
			CameraController camController = m_cam.GetComponent<CameraController> ();
			if (camController)
				camController.enabled = false;
		}
	}

	/// <summary>
	/// Obsługa zdarzenia pozostawania obiektu w kolizji z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerStay(Collider coll)
	{

		if (coll.tag == "Player")
		{
			GameObject player = coll.gameObject;
			player.transform.position = new Vector3(this.transform.position.x, player.transform.position.y + Time.fixedDeltaTime * 5.0f, this.transform.position.z);
			Rigidbody playerRB = player.GetComponent<Rigidbody>();
			if (playerRB)
			{
				playerRB.velocity = Vector3.zero;
			}
		}
	}

	/// <summary>
	/// Obsługa zdarzenia wyjścia obiektu z kolizji z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerExit(Collider coll)
	{
		if (coll.tag == "Player")
		{
			SceneManager.LoadScene(m_nextStage);
		}
	}
}
