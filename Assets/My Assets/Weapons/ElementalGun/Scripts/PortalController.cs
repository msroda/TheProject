﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Kontroler portalu.
/// </summary>
public class PortalController : MonoBehaviour {

	/// <summary>
	/// Drugi portal.
	/// </summary>
	public PortalController m_pair;
	bool m_active; //Czy portal jest aktywny
	bool m_onCooldown; //Czy portal jest tymczasowo dezaktywowany
	/// <summary>
	/// Długość tymczasowej dezaktywacji.
	/// </summary>
	public float m_cooldownTime;
	float m_cooldownTimer; //Licznik czasu dezaktywacji

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		m_active = true;
		m_onCooldown = false;
	}

	/// <summary>
	/// Obsługa liczników czasu.
	/// </summary>
	void HandleTimers()
	{
		m_cooldownTimer += Time.deltaTime;
		if (m_cooldownTimer >= m_cooldownTime)
		{
			m_onCooldown = false;
		}
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update ()
	{
		HandleTimers ();
	}

	/// <summary>
	/// Przenosi zadany obiekt w miejsce portalu-pary.
	/// </summary>
	/// <param name="otherObject">Obiekt do przeniesienia</param>
	void TeleportObject(GameObject otherObject)
	{
		NavMeshAgent agent = otherObject.GetComponent<NavMeshAgent>();
		//W przypadku postaci kierowanych przez SI, musi nastąpić przeniesienie ich agenta;
		if (agent)
		{			
			agent.Warp(m_pair.transform.position + m_pair.transform.rotation * new Vector3 (0.0f, 0.0f, 1.0f));
			agent.SetDestination(m_pair.transform.position + m_pair.transform.rotation * new Vector3 (0.0f, 0.0f, 1.5f));
		}
		otherObject.transform.position = m_pair.transform.position + m_pair.transform.rotation * new Vector3 (0.0f, 0.0f, 1.0f); //Przeniesienie obiektu
		Rigidbody rb = otherObject.GetComponent<Rigidbody>();
		if (rb)
			rb.velocity = rb.velocity.magnitude * m_pair.transform.forward; //Zmiana kierunku prędkości obiektu
		m_pair.SetOnCooldown();
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player"
		   || other.tag == "AIEnemy"
		   || other.tag == "Fireball"
		   || other.tag == "Iceball"
		   || other.tag == "Lightningball")
		{
			if (m_active && !m_onCooldown)
			{
				TeleportObject (other.gameObject);
			}
		}
	}

	/// <summary>
	/// Obsługa zdarzenia wyjśćia obiektu z kolizji z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerExit(Collider other)
	{
		m_onCooldown = false;
	}

	/// <summary>
	/// Aktywuje portal.
	/// </summary>
	public void Activate()
	{
		m_active = true;
	}

	/// <summary>
	/// Tymczasowo dezaktywuje portal.
	/// </summary>
	public void SetOnCooldown()
	{
		m_onCooldown = true;
		m_cooldownTimer = 0.0f;
	}
}
