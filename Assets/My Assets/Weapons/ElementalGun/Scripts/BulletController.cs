﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))] //Kolider pocisku
[RequireComponent(typeof(Rigidbody))] //Reprezentacja pocisku w symulacji fizyki

/// <summary>
/// Kontroler pocisku.
/// </summary>
public class BulletController : MonoBehaviour {

	GameObject m_portalObject; //Obiekt portalu, do przesunięcia gdy ten zderzy się z innym obiektem
	private bool m_immortal = false; //Czy obiekt nie może w danej chwili zostać zniszczony
	private float m_immortalityTimer = 0.0f; //Licznik czasu, w którym obiekt nie może zostać zniszczony
	private Vector3 m_predictedHitPoint; //Punkt, w kierunku którego leci pocisk
	private Vector3 m_predictedHitNormal; //Normalna powierzchni, w kierunku której leci pocisk

	/// <summary>
	/// Obsługa liczników czasu.
	/// </summary>
	void HandleTimers()
	{
		if (m_immortal)
		{
			m_immortalityTimer += Time.deltaTime;
			if (m_immortalityTimer >= 0.2f)
			{
				m_immortal = false;
				m_immortalityTimer = 0.0f;
			}
		}
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update()
	{
		HandleTimers();
	}

	/// <summary>
	/// Przypisuje obiekt portalu, który powinien zostać przemieszczony, gdy pocisk zderzy się z innym obiektem.
	/// </summary>
	/// <param name="newPortal">Portal do przesunięcia.</param>
	public void SetPortal(GameObject newPortal)
	{
		m_portalObject = newPortal;
		RaycastHit hit;
		if (Physics.Raycast (new Ray (this.transform.position, this.transform.forward), out hit))
		{
			m_predictedHitPoint = hit.point;
			m_predictedHitNormal = hit.normal;
		}
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję.
	/// </summary>
	/// <param name="collision">Informacje na temat kolizji.</param>
	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.tag == "Portalable" && m_portalObject)
		{
			Vector3 pos;
			Vector3 normal;
			if (Vector3.Distance (m_predictedHitPoint, collision.contacts [0].point) < 0.5f)
			{
				pos = m_predictedHitPoint;
				normal = m_predictedHitNormal;
			}

			else
			{
				pos = collision.contacts[0].point;
				normal = collision.contacts[0].normal;
			}
			m_immortal = true;
			m_portalObject.transform.position = pos;
			m_portalObject.transform.rotation = Quaternion.LookRotation(normal);
			PortalController tmpPortal = m_portalObject.GetComponent<PortalController>();
			if(tmpPortal)
				tmpPortal.Activate();
			Destroy (this.gameObject);
		}
		else
			if (collision.collider.tag != "Portal" && !m_immortal)
				Destroy (this.gameObject);
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Portal")
		{
			if (m_portalObject)
				Destroy (this.gameObject);
			else
				m_immortal = true;
		}
		else if (!m_immortal)
			Destroy (this.gameObject);
	}
}
