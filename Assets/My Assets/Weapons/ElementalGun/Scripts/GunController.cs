﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof (LineRenderer))] //Do rysowania laserowego celownika

/// <summary>
/// Kontroler broni.
/// </summary>
public class GunController : MonoBehaviour {

	/// <summary>
	/// Obiekty portali.
	/// </summary>
	public GameObject[] m_portals;

	private Quaternion m_targetRotation; //Docelowa rotacja broni
	/// <summary>
	/// Położenie celownika.
	/// </summary>
	public Transform m_aimer;
	/// <summary>
	/// Położenie emitera pocisków.
	/// </summary>
	public Transform m_emiter;
	private LineRenderer m_lineRenderer; //Renderer rysujący laserowy celownik
	/// <summary>
	/// Szablonowe obiekty pocisków.
	/// </summary>
	public GameObject[] m_bullets;
	private int m_bulletType; //Aktualnie wybrany pocisk

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start () {
		m_lineRenderer = GetComponent<LineRenderer>();
		m_lineRenderer.startWidth = 0.004f;
		m_lineRenderer.endWidth = 0.004f;
		m_lineRenderer.enabled = false;
		m_lineRenderer.SetPosition(0, m_aimer.transform.position);
		m_lineRenderer.SetPosition(1, m_aimer.transform.position);
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		transform.rotation = Quaternion.RotateTowards(transform.rotation, m_targetRotation, 360 * Time.fixedDeltaTime);
	}

	/// <summary>
	/// Ustala docelową otację obiektu.
	/// </summary>
	/// <param name="targetPos">Punkt, w kierunku którego powinien być skierowany obiekt.</param>
	public void SetTargetRotation(Vector3 targetPos)
	{
		if(targetPos.magnitude > 0.1f)
			m_targetRotation = Quaternion.LookRotation (targetPos);
	}

	/// <summary>
	/// Wybiera typ pocisków, które powinny być generowane.
	/// </summary>
	/// <param name="typeNumber">ID typu.</param>
	public void SetBulletType(int typeNumber)
	{
		if (typeNumber < m_bullets.Length)
		{
			m_bulletType = typeNumber;
		}
	}

	/// <summary>
	/// Obraca broń, by celowała w wyznaczony punkt.
	/// </summary>
	/// <returns><c>true</c>, jeśli broń mogła wycelować w punkt, <c>false</c> jeśli punkt był zasłonięty przez inny obiekt.</returns>
	/// <param name="aimedPoint">Punkt docelowy.</param>
	/// <param name="enableLaserSight">Jeśli <c>true</c>, zostanie narysowany laserowy celownik.</param>
	public bool AimTo(Vector3 aimedPoint, bool enableLaserSight)
	{
		RaycastHit hit;
		float distance = Vector3.Distance (m_aimer.transform.position, aimedPoint);
		bool canShoot;

		//Jeśli docelowy punkt jest czymś zasłonięty, celuj w przeszkodę
		if (Physics.Raycast (m_aimer.transform.position, aimedPoint - m_aimer.transform.position, out hit, distance))
		{
			m_lineRenderer.SetPosition(0, m_aimer.transform.position);
			m_lineRenderer.SetPosition(1, hit.point);
			SetTargetRotation(hit.point - this.transform.position);
			canShoot = false;
		}
		//Jeśli nie, celuj w punkt
		else
		{
			m_lineRenderer.SetPosition(0, m_aimer.transform.position);
			m_lineRenderer.SetPosition(1, aimedPoint);
			SetTargetRotation(aimedPoint - this.transform.position);
			canShoot = true;
		}

		if(enableLaserSight && Vector3.Angle(this.transform.forward, aimedPoint - m_aimer.transform.position) <= 15.0f)
			m_lineRenderer.enabled = true;
		else
			m_lineRenderer.enabled = false;
		
		return canShoot;

	}

	/// <summary>
	/// Generuje instancję pocisku i nadaje jej prędkość.
	/// </summary>
	/// <param name="time">Czas życia pocisku.</param>
	/// <param name="force">Siła wystrzału.</param>
	public void Shoot(float time, float force)
	{
		GameObject tempBullet;
		tempBullet = Instantiate(m_bullets[m_bulletType], m_emiter.transform.position, m_emiter.transform.rotation) as GameObject;

		Rigidbody tempBulletRigidbody;
		tempBulletRigidbody = tempBullet.GetComponent<Rigidbody>();
		BulletController tempBulletController;
		tempBulletController = tempBullet.GetComponent<BulletController>();

		//Jeśli pocisk ma przenosić portal, należy do niego przypisać referencję na odpowiedni obiekt portalu
		if (m_bulletType == 3)
			tempBulletController.SetPortal(m_portals [0]);
		if (m_bulletType == 4)
			tempBulletController.SetPortal(m_portals [1]);

		tempBulletRigidbody.AddForce (m_emiter.transform.rotation * new Vector3 (0.0f, 0.0f, force), ForceMode.Impulse);

		Destroy(tempBullet, time); //Zniszczenie obiektu z opóźnieniem czasowym
	}
}
