﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (GunController))] //Broń do kontrolowania

/// <summary>
/// Kontroler broni (Gracza).
/// </summary>
public class PlayerGunController : MonoBehaviour {

	private Transform m_cam; //Główna kamera
	/// <summary>
	/// Obiekt gracza.
	/// </summary>
	public GameObject m_player;
	/// <summary>
	/// Maksymalna odległość celowania.
	/// </summary>
	public float m_aimDistance = 200.0f;
	private GunController m_gun; //Broń do kontrolowania
	/// <summary>
	/// Siła wystrzału.
	/// </summary>
	public float m_shootingForce = 20.0f;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		if (Camera.main != null)
		{
			m_cam = Camera.main.transform;
		}
		m_gun = GetComponent<GunController>();
	}

	/// <summary>
	/// Wczytanie informacji od gracza.
	/// </summary>
    void GetUserInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            m_gun.SetBulletType(0);
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            m_gun.SetBulletType(1);
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            m_gun.SetBulletType(2);
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            m_gun.SetBulletType(3);
        else if (Input.GetKeyDown(KeyCode.Alpha5))
            m_gun.SetBulletType(4);
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            m_gun.SetBulletType(5);
        else if (Input.GetKeyDown(KeyCode.Alpha7))
            m_gun.SetBulletType(6);
        else if (Input.GetKeyDown(KeyCode.Alpha8))
            m_gun.SetBulletType(7);
        else if (Input.GetKeyDown(KeyCode.Alpha9))
            m_gun.SetBulletType(8);
        else if (Input.GetKeyDown(KeyCode.Alpha0))
            m_gun.SetBulletType(9);
    }

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
    void Update()
	{
        GetUserInput();
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		//Jeśli gracz celuje
		if (Input.GetMouseButton(1))
		{
            RaycastHit hit;
			//Jeśli punkt wskazywany przez kamerę mieści się w granicy, celuj w niego
			if (Physics.Raycast (m_cam.transform.position, m_cam.transform.forward, out hit, m_aimDistance))
			{
				m_gun.AimTo(hit.point, true);
			}
			//W przeciwnym razie, celuj w punkt graniczny
			else
			{
				m_gun.AimTo(m_cam.transform.position + m_cam.transform.forward.normalized * m_aimDistance, true);
			}

			//Jeśli gracz chce strzelić
			if (Input.GetMouseButtonDown(0))
			{
				m_gun.Shoot(5.0f, m_shootingForce);
			}

		}
		//Celuj w przód
		else
		{
			m_gun.AimTo(this.transform.position + m_player.transform.forward, false);
		}
	}
}
