﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// Kontroler kamery.
/// </summary>
public class CameraController : MonoBehaviour {

	/// <summary>
	/// Pozycja celu do śledzenia.
	/// </summary>
	public Transform m_target;

	private float m_sensivity; //Czułość myszy

	/// <summary>
	/// Przesunięcie względem m_target.
	/// </summary>
	public Vector3 m_fixedTargetOffset = Vector3.zero;

	/// <summary>
	/// Maksymalne oddalenie kamery.
	/// </summary>
	public float m_maxZoom = 3.0f;

	/// <summary>
	/// Maksymalne zbliżenie kamery.
	/// </summary>
	public float m_minZoom = 1.5f;

	private float m_currentZoom = 2.0f; //Aktualne oddalenie kamery

	/// <summary>
	/// Prędkość zbliżanai kamery.
	/// </summary>
	public float m_zoomingSpeed = 70.0f; //Prędkość zbliżanai kamery

	/// <summary>
	/// Maksymalny obrót kamery względem osi X.
	/// </summary>
	public float m_maxXRotation = 80.0f;

	/// <summary>
	/// Minimalny obrót kamery względem osi X.
	/// </summary>
	public float m_minXRotation = -80.0f;

	private float m_currentXRotation = -45.0f; //Obecny obrót kamery względem osi X
	private float m_currentYRotation = 0.0f; //Obecny obrót kamery względem osi Y

	/// <summary>
	/// Prędkość obracania kamery wględem osi X.
	/// </summary>
	public float m_rotateSpeedX = 150.0f;

	/// <summary>
	/// Prędkość obracania kamery względem osi Y.
	/// </summary>
	public float m_rotateSpeedY = 150.0f;

	private Vector3 m_camvel = Vector3.zero; //Prędkość kamery. W praktyce zmienna nieużywana, jednak jedna z funkcji wymaga podania referencji w parametrze

	/// <summary>
	/// Przesunięćie kamery podczas celowania.
	/// </summary>
	public Vector3 m_aimingOffset = new Vector3 (-0.5f, 0.0f, 0.5f);

	private Vector3 m_fixedTargetPos = Vector3.zero; //Właściwy punkt-cel, wyliczony na podstawie m_target oraz m_fixedTargetOffset
	private Vector3 m_desiredCameraPos = Vector3.zero; //Wstępna pozycja kamery, na podstawie informacji użytkownika
	private Vector3 m_adjustedCameraPos = Vector3.zero; //Pozycja kamery dopasowana do ewentualnych kolidujących obiektów
	private Vector3[] m_desiredCamPoints; //Punkty kamery, do sprawdzania, czy między nią a celem nie ma przeszkody
	float m_adjustedZoom; //Dopasowana odległość kamery od celu

	float m_shiftState = 0.0f; //Aktualne przesunięcie kamery, do płynnej zmiany trybu między klasycznym a celowaniem

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start()
	{
		SetTarget(m_target);

		m_sensivity = 1.0f;

		FollowTarget();

		UpdateCamPoints(m_desiredCameraPos, transform.rotation, ref m_desiredCamPoints);

		Cursor.lockState = CursorLockMode.Confined;
	}

	/// <summary>
	/// Wczytanie informacji od gracza.
	/// </summary>
	void GetUserInput()
	{
		m_currentXRotation -= CrossPlatformInputManager.GetAxis("Mouse Y") * m_rotateSpeedX * Time.deltaTime * m_sensivity;
		m_currentYRotation += CrossPlatformInputManager.GetAxis("Mouse X") * m_rotateSpeedY * Time.deltaTime * m_sensivity;
		m_currentZoom -= CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") * m_zoomingSpeed * Time.deltaTime;

		if (Input.GetKeyDown (KeyCode.Plus) || Input.GetKeyDown (KeyCode.KeypadPlus))
			m_sensivity = Mathf.Clamp (m_sensivity + 0.5f, 0.5f, 4.0f);

		if (Input.GetKeyDown (KeyCode.Minus) || Input.GetKeyDown (KeyCode.KeypadMinus))
			m_sensivity = Mathf.Clamp (m_sensivity - 0.5f, 0.5f, 4.0f);

		m_currentXRotation = Mathf.Clamp(m_currentXRotation, m_minXRotation, m_maxXRotation);
		m_currentZoom = Mathf.Clamp(m_currentZoom, m_minZoom, m_maxZoom);

		if(Input.GetMouseButton(1))
			m_shiftState += Time.deltaTime * 5;
		else
			m_shiftState -= Time.deltaTime * 5;

		m_shiftState = Mathf.Clamp (m_shiftState, 0.0f, 1.0f);
	}

	/// <summary>
	/// Ustalenie celu do śledzenia.
	/// </summary>
	/// <param name="newTarget">Nowy celt.</param>
	void SetTarget(Transform newTarget)
	{
		m_target = newTarget;
	}

	/// <summary>
	/// Wywoływana przy każdej aktualizacji silnika fizyki.
	/// </summary>
	void FixedUpdate()
	{
		GetUserInput();

		m_adjustedZoom = -1.0f;
		FollowTarget(); //Wstępne ustawienie kamery
		LookAtTarget();

		UpdateCamPoints(m_desiredCameraPos, transform.rotation, ref m_desiredCamPoints);
		m_adjustedZoom = GetFixedDistance(m_fixedTargetPos);

		FollowTarget(); //Ustawienie drugi raz, z uwzględnieniem ewentualych przeszkód na drodze kamera-cel
		LookAtTarget();
	}

	/// <summary>
	/// Ustala pozycję kamery względem pozycji celu.
	/// </summary>
	void FollowTarget()
	{
		m_fixedTargetPos = m_target.position + m_fixedTargetOffset;
		m_desiredCameraPos = Quaternion.Euler(m_currentXRotation, m_currentYRotation, 0.0f) * -Vector3.forward * m_currentZoom + m_fixedTargetPos + transform.rotation * m_aimingOffset * m_shiftState;

		if (m_adjustedZoom != -1.0f)
		{
			m_adjustedCameraPos = Quaternion.Euler(m_currentXRotation, m_currentYRotation, 0.0f) * -Vector3.forward * m_adjustedZoom + m_fixedTargetPos + transform.rotation * m_aimingOffset * m_shiftState;
			transform.position = Vector3.SmoothDamp(transform.position, m_adjustedCameraPos, ref m_camvel, 0.1f);
		}
		else
		{
			transform.position = Vector3.SmoothDamp(transform.position, m_desiredCameraPos, ref m_camvel, 0.1f);
		}
	}

	/// <summary>
	/// Odwraca kamerę, by patrzyła na cel.
	/// </summary>
	void LookAtTarget()
	{
		Quaternion targetRotation = Quaternion.LookRotation (m_fixedTargetPos + transform.rotation * m_aimingOffset * m_shiftState - transform.position);
		this.transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, 30 * Time.deltaTime);
	}
		
	/// <summary>
	/// Aktualizuje punkty kamery na podstawie jej pozycji oraz rotacji.
	/// </summary>
	/// <param name="camPosition">Pozycja kamery.</param>
	/// <param name="camRotation">Rotacja kamery.</param>
	/// <param name="output">Zmienna do zapisu nowych punktów.</param>
	void UpdateCamPoints(Vector3 camPosition, Quaternion camRotation, ref Vector3[] output)
	{
		Camera cam = Camera.main;

		if(!cam)
			return;

		output = new Vector3[5];

		float z = cam.nearClipPlane;
		float x = Mathf.Tan(cam.fieldOfView / 3.41f) * z;
		float y = x / cam.aspect;

		output [0] = (camRotation * new Vector3 (-x, y, z)) + camPosition;
		output [1] = (camRotation * new Vector3 (x, y, z)) + camPosition;
		output [2] = (camRotation * new Vector3 (-x, -y, z)) + camPosition;
		output [3] = (camRotation * new Vector3 (x, -y, z)) + camPosition;
		output [4] = (camRotation * new Vector3 (0.0f, 0.0f, z)) + camPosition;
	}

	/// <summary>
	/// Oblicza, jak daleko od celu, powinna znajdować się kamera, by mogła go widzieć.
	/// </summary>
	/// <returns>Wyliczoną odległość kamery od celu.</returns>
	/// <param name="targetPos">Pozycja celu.</param>
	float GetFixedDistance(Vector3 targetPos)
	{
		float distance = -1;

		for (int i = 0; i < m_desiredCamPoints.Length; i++)
		{
			Ray ray = new Ray(targetPos, m_desiredCamPoints[i] - targetPos);
			float maxDistance = Vector3.Distance (m_desiredCamPoints [i], targetPos);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, maxDistance))
			{
				if (distance == -1)
					distance = hit.distance * Vector3.Distance(m_desiredCamPoints[4], targetPos) / Vector3.Distance(m_desiredCamPoints[i], targetPos);
				else
					distance = Mathf.Min(distance, hit.distance * Vector3.Distance(m_desiredCamPoints[4], targetPos) / Vector3.Distance(m_desiredCamPoints[i], targetPos));
			}
		}
		return distance;
	}
}
