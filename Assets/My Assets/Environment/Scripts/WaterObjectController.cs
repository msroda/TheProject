﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Kontroler interaktywnej wody.
/// </summary>
public class WaterObjectController : MonoBehaviour {

	/// <summary>
	/// Stan skupienia wody.
	/// </summary>
	public enum WaterState
	{
		/// <summary>
		/// Stan ciekły.
		/// </summary>
		LIQUID,
		/// <summary>
		/// Stan stały.
		/// </summary>
		FROZEN
	}

	/// <summary>
	/// Obiekt wody w stanie ciekłym.
	/// </summary>
	public GameObject m_liquidObject;
	/// <summary>
	/// Obiekt wody w stanie stałym.
	/// </summary>
	public GameObject m_frozenObject;
	/// <summary>
	/// Aktualny stan obiektu wody.
	/// </summary>
	public WaterState m_state;

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		switch (m_state)
		{
		case WaterState.LIQUID:
			m_liquidObject.SetActive(true);
			break;
		case WaterState.FROZEN:
			m_frozenObject.SetActive(true);
			break;
		}
	}

	/// <summary>
	/// Obsługa zdarzenia wejśćia obiektu w kolizję z wyzwalaczem.
	/// </summary>
	/// <param name="coll">Kolider drugiego obiektu.</param>
	void OnTriggerEnter(Collider other)
	{
		switch (other.tag)
		{
		case "Fireball":
            ShowWater();
            break;
		case "Iceball":
            ShowIce();
			break;
		}
	}

	/// <summary>
	/// Uaktywnia obiekt wody w stanie ciekłym.
	/// </summary>
    void ShowWater()
    {
        m_liquidObject.SetActive(true);
        m_frozenObject.SetActive(false);
    }
       
	/// <summary>
	/// Uaktywnia obiekt wody w stanie stałym.
	/// </summary>
	void ShowIce()
    {
        m_liquidObject.SetActive(false);
        m_frozenObject.SetActive(true);
    }
}
