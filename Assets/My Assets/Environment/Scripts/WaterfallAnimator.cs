﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Animator tekstury wodospadu.
/// </summary>
public class WaterfallAnimator : MonoBehaviour {

	/// <summary>
	/// Prędkość animacji.
	/// </summary>
	public float m_speed;
	private Renderer m_renderer; //Renderer tekstury
	private float m_offset; //Krok przesunięcia tekstury

	/// <summary>
	/// Inicjalizacja obiektu.
	/// </summary>
	void Start ()
	{
		m_renderer = GetComponent<Renderer> ();
		m_offset = 0.0f;
	}

	/// <summary>
	/// Wywołana co klatkę.
	/// </summary>
	void Update ()
	{
		m_offset = (m_offset + Time.deltaTime * m_speed) % 1;
		m_renderer.material.SetTextureOffset ("_MainTex", new Vector2 (0.0f, m_offset));
	}
}
